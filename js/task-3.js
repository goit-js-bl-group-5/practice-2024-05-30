/*
Створити Калькулятор за допомогою конструктора
https://uk.javascript.info/task/calculator-constructor

Створіть функцію-конструктор Calculator, який створює об’єкти з трьома методами:

    read() запитує два значення за допомогою prompt і записує їх у властивості об’єкта з іменами a та b.
    sum() повертає суму цих властивостей.
    mul() повертає результат множення даних властивостей.
*/

// const calculator = new Calculator();
// calculator.read();

// console.log( "Sum=" + calculator.sum() );
// console.log( "Mul=" + calculator.mul() );
