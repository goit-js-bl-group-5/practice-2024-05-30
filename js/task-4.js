/*
Створити Accumulator
https://uk.javascript.info/task/accumulator

Створіть функцію-конструктор Accumulator(startingValue).

Об’єкт, який він створює повинен:

    Зберігати “поточне значення” у властивості value. Початкове значення має значення аргументу конструктора startingValue.
    Метод read() повинен використовувати prompt для зчитування нового числа та додавати його до value.

Іншими словами, властивість value – це сума всіх введених користувачем значень разом із початковим значенням startingValue.
*/

// const accumulator = new Accumulator(1); // початкове значення 1

// accumulator.read(); // додає введене користувачем значення
// accumulator.read(); // додає введене користувачем значення

// console.log(accumulator.value); // показує суму цих значень
