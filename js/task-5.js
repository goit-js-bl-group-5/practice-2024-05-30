/*
Помилка створення екземпляра
https://uk.javascript.info/task/class-constructor-error

Ось код з Rabbit розширює Animal.

На жаль, неможливо створити об’єкти Rabbit. Що не так? Полагодьте це.
*/

// class Animal {

//   constructor(name) {
//     this.name = name;
//   }

// }

// class Rabbit extends Animal {
//   constructor(name) {
//     this.name = name;
//     this.created = Date.now();
//   }
// }

// const rabbit = new Rabbit("White Rabbit"); // Error: this is not defined
// console.log(rabbit.name);
