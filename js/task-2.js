/*
Створіть калькулятор
https://uk.javascript.info/task/calculator

Створіть об’єкт calculator з трьома методами:

    read() запитує два значення та зберігає їх як властивості об’єкта з іменами a та b відповідно.
    sum() повертає суму збережених значень.
    mul() множить збережені значення і повертає результат.
*/

// const calculator = {
//   /* ... ваш код */
// };

// calculator.read();
// console.log( calculator.sum() );
// console.log( calculator.mul() );
